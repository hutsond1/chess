# Chess? #

A new twist on a game as old as games. 

![Level 1](img/dungeon.png)

# The Plan #

For our project we originally planned on creating a game like chess but with custom pieces, board size, and 3D animation. After reviewing what we knew how to do, the team decided it would be best to pivot a little bit and create something similar that was more suited to our skill set. 

# The New Plan #

Given that none of us knew how to make a point-&-click game, but we did know how to make a platformer; instead of throwing out all of our assets, we just took the models we had, added some character interactions and built some levels based on a castle theme. Like chess, the player wins by taking the king piece, but since this is a platform style game (instead of a turn-based game), the player has to avoid the enemies in real-time in order to not lose their piece.


## How do I get set up? ##

1. Download the .blend file
1. Open in blender
1. Press P


## Controls ##

* WASD to move
* Enter to interact
* Space to jump


## Creators ##

* Connor Klawon - models, game tester, game logic
* Anneliese Friedlander - models, level design
* Daniel Hutson - models, animations, troubleshooting
* Joseph Lamosek - models, writer, moral support


